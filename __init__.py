import os
from flask import Flask
import threading



def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dcp',
        DATABASE=os.path.join(app.instance_path, 'webserver.sqlite'),
    )

    #app.config.from_pyfile('config.py', silent=True)
    
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from webserver import db
    db.init_app(app)
    
    from webserver import webpage
    app.register_blueprint(webpage.bp)

    return app
