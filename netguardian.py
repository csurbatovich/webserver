import socket
import os
import sqlite3
import time
import sys
import smtplib
import struct
#range
#bit 0 - 2 range 0 - 4

# incoming value is 

# end of nvram
# ngopts location -8 bytes
# 0x20000000ul + 0xffffu // start/end ngopts
# - 8 start
# 16 calSet   set to A5 when calibrated   1 byte
# 17 5   2-byte ints for slope            10 bytes
# 27 5 1-byte values for y intercept      5 bytes
# - 8 - 16 end
#ranges
# 0 - 5.5
# 5.5 - 14
# 14 - 30
# 30 - 70
# [AA][FA][09][00][C3][0A][63][97][AA][FA][0A][01][C3][0A][5E][09]
#  -48.39                                          -48.30

# 
#
scaling = [0.001522821,0.003863678,0.008098398,0.018197650,0.02306719,1.0,1.0,1.0]
calibrationOff = 0x55
calibrationOn = 0xA5
ngoptsAddress = 0xFFC0
addressH = 0xFF
addressL = 0xC0
checksum = 0
ngoptsSize = 64
calSet = 55


def poll_ng(netguardian) :
    #create fudr poll
    print('Polling NetGuardian...',file=sys.stderr)
    message = []
    message.append(0xAA)
    message.append(0xFC)
    message.append(int(netguardian['address']))
    message.append(0x03)
    message.append(calcBCH(message,4))
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('',int(netguardian['port'])))
    sock.settimeout(1)
    sock.sendto(bytes(message),(netguardian['ip'],int(netguardian['port'])))

    sock.settimeout(5)
    #read response
    temp = bytearray(10)
    sock.recv_into(temp)
    numLines = temp[3]
    lineNumber = 1
    while lineNumber < 4 :
        temp = bytearray(10)
        sock.recv_into(temp)
        lineNumber = temp[2]

    analogReading = 0.0;
    channel = 0
    while channel < netguardian['channels'] :
        response = bytearray(10)
        sock.recv_into(response)
        if response[1] > 0 :    
            scalingRange = response[4]&0x0F
            temp = int.from_bytes(response[5:7],byteorder='big')
            analogReading = analogReading + float(temp * scaling[scalingRange]) 
        channel = channel + 1
    #extract analog values and insert to database
    #analogs start at display 3, line 5
    analogReading = analogReading / netguardian['channels']
    timeStamp = time.time()
    sock.close()
    print('Read',analogReading,file=sys.stderr)
    return (analogReading, timeStamp)


def write_ng(netguardian):
    message = bytearray()
    message.append(0xAA)
    message.append(0xFC)
    message.append(int(netguardian['address']))
    message.append(0xFB)
    message.append(0xB0)
    message.append(2 + ngoptsSize)
    message.append(addressH)
    message.append(addressL)
   # message.append(ngoptsSize)
    i = 0
    while i < ngoptsSize :
        message.append(netguardian['ngopts'][i])
        i = i + 1
    message.append(calcBCH(message,len(message)))
    
    print("Writing to ng",file=sys.stderr)
    #   print(message,file=sys.stderr)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)
    sock.bind(('',int(netguardian['port'])))
    sock.sendto(bytes(message),(netguardian['ip'],int(netguardian['port'])))
    time.sleep(1)
    sock.close()

def restore(netguardian):
    netguardian['ngopts'] = [0xea,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x76,0x75,0x00,0x81,0x10,0x00,0x55,0x02,0x00,0x0c,0x15,0x30,0x26,0xac,0x26,0xbb,0x26,0x8e,0x26,0x72,0x26,0x4a,0xa5,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff]
    
def loadNGOPTS(netguardian):
    print('Loading ngopts from NetGuardian',file = sys.stderr)
    message = bytearray()
    message.append(0xAA)
    message.append(0xFC)
    message.append(int(netguardian['address']))
    message.append(0xFB)
    message.append(0xB2)
    message.append(0x03)
    message.append(addressH)
    message.append(addressL)
    message.append(ngoptsSize)
    message.append(calcBCH(message,len(message)))
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('',int(netguardian['port'])))
    sock.sendto(bytes(message),(netguardian['ip'],int(netguardian['port'])))
    sock.settimeout(10)
    #read response

    netbuffer = bytearray(74)
    sock.recv_into(netbuffer)
    netguardian['ngopts'] = netbuffer[9:73]
    time.sleep(1)
    sock.close()
    
def disableCalibration(netguardian):
    print('Turning off calibration',file=sys.stderr)
    netguardian['ngopts'][calSet] = calibrationOff
    netguardian['ngopts'][checksum] = calcChecksum(netguardian['ngopts'])
                                                            
def enableCalibration(netguardian) :
    print('Turning on calibration',file=sys.stderr)
    netguardian['ngopts'][calSet] = calibrationOn
    netguardian['ngopts'][checksum] = calcChecksum(netguardian['ngopts'])
    
def writeCalibrationValues(netguardian,values) :
    print('Writing Calibration Values',file=sys.stderr)
    i = 0
    while i < 15 :
        netguardian['ngopts'][ngoptsSize - (10 + i)] = values[i]
        i = i + 1

def calcChecksum(ngopts):
    cs = 0
    i = 1
    while i < ngoptsSize :
        cs = cs + ngopts[i]
        i = i + 1
    return (0xFF - (cs & 0xFF)) + 1
    #return 0xFF & cs
    
def calcBCH(buffer,length):
    bch = 0x00
    f_poly = 0xFF
    n_poly = 0xB8

    i = 0
    while i < length :
        bch ^= buffer[i]
        j = 0
        while j < 8 :
            if (bch&1) == 1 :
                bch = (bch >> 1) ^ n_poly
            else :
                bch >>= 1
            j = j + 1
        i = i + 1
    bch ^= f_poly

    return bch
