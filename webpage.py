import functools
import struct
import os
import sys
import time
import threading
import socket
from flask import(
    Blueprint, flash, g, redirect, render_template, request, session, url_for, Response, Flask, send_file
)


from werkzeug.security import check_password_hash, generate_password_hash

from webserver.db import get_db

from webserver.powersupply import setValue, readValue, savePlot, VOLTAGE, CURRENT, powerOn, powerOff

from webserver.netguardian import poll_ng, write_ng, loadNGOPTS, disableCalibration, enableCalibration, writeCalibrationValues, restore

bp = Blueprint('rtu', __name__)

WAIT = 3
calibrationValues = bytearray(15)
readingID = 0
index = 0
currentValue = 0.1
netGuardian = {} 
powerSupply = {}
rangeIndex = 0
calibrationRanges = {
    0:2.5,
    1:1.0,
    2:5.0,
    3:10.5,
    4:6.0,
    5:13.0,
    6:22.0,
    7:15.0,
    8:29.0,
    9:50.0,
    10:32.0,
    11:68.0,
    12:75.0,
    13:71.0,
    14:80.0,
    }


powerSupply['calibrated'] = True
powerSupply['slope'] = 0.9997674418604652 
powerSupply['yint'] = -0.2199488372093012
powerSupply['external'] = False
netGuardian['ip'] =  "126.10.218.10"
netGuardian['port'] = 2003
netGuardian['address'] = 1
netGuardian['channels'] = 4
powerSupply['address'] = 'A'
powerSupply['ip'] = "126.10.218.101"
powerSupply['port'] = 1056
powerSupply['target'] = 0
calibrationData = []
psuVoltageReadings = []
ngVoltageReadings = []
labels = []
readingLimit = 10
calibrationOn = False
calibrationStep = 0
calibrationCycle = 0
calibrationRange = 0
calibrationStage = 0
valueToSet = 0
timeStamps = []
measurePsu = False
measureNGD = False
targetVoltage = 0
ngHigh = 0
ngLow = 0
psHigh = 0
psLow = 0
tolerance = 0.5
startTime = 0
maxCount = 3
currentCount = 0
testingData = [{},{},{},{},{}]
calibrationData = [{},{},{},{},{}]
z = 0
while z < 5 :
    calibrationData[z] = {
        'PSL':0,
        'PSH':0,
        'NGL':0,
        'NGH':0,
        'SLP':0,
        'INT':0,
        }
    testingData[z] = {
        'actual':0,
        'netguardian':0,
        'error':0
        }
    z = z + 1



@bp.route('/chart',methods=('GET','POST'))
def chart():
    global calibrationStage
    global calibrationCycle
    global valueToSet
    global rangeIndex
    global targetVoltage
    global tolerance
    global ngHigh
    global ngLow
    global psHigh
    global psLow
    global index
    global readingLimit
    global startTime
    global maxCount
    global currentCount
    (ngReading,ngTime) = poll_ng(netGuardian) 
    labels.append(int(ngTime - startTime))
    ngVoltageReadings.append(ngReading)
    (psReading,psTime) = readValue(powerSupply,VOLTAGE)
    psuVoltageReadings.append(psReading)
    print("calibrationStage: ",calibrationStage,file=sys.stderr)
    # set middle voltage
    if calibrationStage == 2 :
        targetVoltage = calibrationRanges[rangeIndex]
        print("Setting voltage to: ",targetVoltage,file=sys.stderr)
        rangeIndex = rangeIndex + 1                                                
        setValue(powerSupply,currentValue,CURRENT)
        setValue(powerSupply,targetVoltage,VOLTAGE)
        powerOn(powerSupply)
        calibrationStage = 3
    #wait for voltage to be close
    if (calibrationStage == 3) and ((abs(psuVoltageReadings[len(psuVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)) :
        calibrationStage = 4
        currentCount = 0
    elif calibrationStage == 3 :
        currentCount = currentCount + 1
    if (calibrationStage == 4) and ((abs(ngVoltageReadings[len(ngVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)):     
        currentCount = 0
        #save low voltages
        if valueToSet == 0 :
            valueToSet = 1
            calibrationStage = 2
        elif valueToSet == 1 :
            psLow = psuVoltageReadings[len(psuVoltageReadings) - 1]
            ngLow = ngVoltageReadings[len(ngVoltageReadings) - 1]
            calibrationStage = 2
            valueToSet = 2
        #save high voltages
        elif valueToSet == 2 :
            psHigh = psuVoltageReadings[len(psuVoltageReadings) - 1]
            ngHigh = ngVoltageReadings[len(ngVoltageReadings) - 1]
            calibrationStage = 5
            valueToSet = 0
    elif calibrationStage == 4 :
        currentCount = currentCount + 1

    #save calibration
    if calibrationStage == 5 : 
        x = psHigh - psLow
        y = ngHigh - ngLow
        slope = y / x
        yint = (psHigh - ngHigh) * slope
        slope = slope * 10000
        calibrationValues[index] = (int(slope) >> 8) & 0xFF
        calibrationValues[index + 1] = int(slope) & 0xFF
        calibrationValues[10 + int(index/2)] = int(yint * 100) & 0xFF
        index = index + 2
        calibrationData[calibrationCycle]['PSL'] = psLow                
        calibrationData[calibrationCycle]['PSH'] = psHigh
        calibrationData[calibrationCycle]['NGL'] = ngLow
        calibrationData[calibrationCycle]['NGH'] = ngHigh
        calibrationData[calibrationCycle]['SLP'] = slope/10000
        calibrationData[calibrationCycle]['INT'] = yint
        calibrationCycle = calibrationCycle + 1
        calibrationStage = 2
    if calibrationCycle > 4 :
        return redirect(url_for('rtu.success'))

    ngGraph = [] 
    psGraph = []
    times = []
    ngLength = len(ngVoltageReadings)
    if ngLength > readingLimit :
        ngGraph = ngVoltageReadings[ngLength - readingLimit : ngLength]
        psGraph = psuVoltageReadings[ngLength - readingLimit : ngLength]
        times = labels[ngLength - readingLimit : ngLength]
    else:
        ngGraph = ngVoltageReadings
        psGraph = psuVoltageReadings
        times = labels
    return render_template('home/chart.html', data = calibrationData, values = ngGraph, values2 = psGraph, labels = times)



def resetCalibrationData():
    global index
    index = 0
    i = 0
    while i < 5 :
        calibrationData[i]['PSL'] = 0
        calibrationData[i]['PSH'] = 0
        calibrationData[i]['NGL'] = 0
        calibrationData[i]['NGH'] = 0
        calibrationData[i]['SLP'] = 0
        calibrationData[i]['INT'] = 0
        i = i + 1

def resetTestData():
    global index
    index = 0
    i = 0
    while i < 5 :
        calibrationData[i]['actual'] = 0
        calibrationData[i]['netguardian'] = 0
        calibrationData[i]['error'] = 0
        i = i + 1

@bp.route('/calibrate',methods=('GET','POST'))
def calibrate():
    global calibrationCycle
    global calibrationStage
    global valueToSet
    global rangeIndex
    global startTime
    global currentCount
    resetCalibrationData()
    powerOn(powerSupply)
    loadNGOPTS(netGuardian)
    disableCalibration(netGuardian)
    write_ng(netGuardian)
    calibrationStage = 2
    calibrationCycle = 0
    valueToSet = 0
    rangeIndex = 0
    currentCount = 0
    startTime = time.time()
    return render_template('home/calibrate.html')

@bp.route('/results',methods=('GET','POST'))
def results():
    setValue(powerSupply,5.0,VOLTAGE)
    powerOff(powerSupply)
    return render_template('home/results.html',data = testingData)

@bp.route('/autotesting',methods=('GET','POST'))
def autotestloop():
    global calibrationStage
    global calibrationCycle
    global valueToSet
    global rangeIndex
    global targetVoltage
    global tolerance
    global ngHigh
    global ngLow
    global psHigh
    global psLow
    global index
    global readingLimit
    global startTime
    global maxCount
    global currentCount
    (ngReading,ngTime) = poll_ng(netGuardian) 
    labels.append(int(ngTime - startTime))
    ngVoltageReadings.append(ngReading)
    (psReading,psTime) = readValue(powerSupply,VOLTAGE)
    psuVoltageReadings.append(psReading)
    print("calibrationStage: ",calibrationStage,file=sys.stderr)
    # set middle voltage
    if calibrationStage == 2 :
        targetVoltage = calibrationRanges[rangeIndex]
        print("Setting voltage to: ",targetVoltage,file=sys.stderr)
        rangeIndex = rangeIndex + 3                                                
        setValue(powerSupply,currentValue,CURRENT)
        setValue(powerSupply,targetVoltage,VOLTAGE)
        powerOn(powerSupply)
        calibrationStage = 3
    #wait for voltage to be close
    if (calibrationStage == 3) and ((abs(psuVoltageReadings[len(psuVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)) :
        calibrationStage = 4
        currentCount = 0
    elif calibrationStage == 3 :
        currentCount = currentCount + 1
    if (calibrationStage == 4) and ((abs(ngVoltageReadings[len(ngVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)):     
        currentCount = 0
        psLow = psuVoltageReadings[len(psuVoltageReadings) - 1]
        ngLow = ngVoltageReadings[len(ngVoltageReadings) - 1]
        calibrationStage = 5
    elif calibrationStage == 4 :
        currentCount = currentCount + 1

    #save calibration
    if calibrationStage == 5 : 
        testingData[calibrationCycle]['actual'] = psLow
        testingData[calibrationCycle]['netguardian'] = ngLow
        testingData[calibrationCycle]['error'] = ((ngLow - psLow) / psLow) * 100
        calibrationCycle = calibrationCycle + 1
        calibrationStage = 2
    if calibrationCycle > 4 :
        return redirect(url_for('rtu.results'))

    ngGraph = [] 
    psGraph = []
    times = []
    ngLength = len(ngVoltageReadings)
    if ngLength > readingLimit :
        ngGraph = ngVoltageReadings[ngLength - readingLimit : ngLength]
        psGraph = psuVoltageReadings[ngLength - readingLimit : ngLength]
        times = labels[ngLength - readingLimit : ngLength]
    else:
        ngGraph = ngVoltageReadings
        psGraph = psuVoltageReadings
        times = labels
    return render_template('home/autotest.html', data = testingData, values = ngGraph, values2 = psGraph, labels = times)

@bp.route('/autotest',methods=('GET','POST'))
def autotest():
    powerOn(powerSupply)
    resetTestData()
    global calibrationCycle
    global calibrationStage
    global valueToSet
    global rangeIndex
    global startTime
    global currentCount
    calibrationStage = 2
    calibrationCycle = 0
    valueToSet = 0
    rangeIndex = 0
    currentCount = 0
    startTime = time.time()
    return render_template('home/autotesthome.html')

@bp.route('/enable',methods=('GET','POST'))
def enable():
    print('Enabling Calibration',file=sys.stderr)
    loadNGOPTS(netGuardian)
    enableCalibration(netGuardian)
    write_ng(netGuardian)
    flash('Enabled Calibration')
    return redirect(url_for('rtu.test'))

@bp.route('/disable',methods=('GET','POST'))
def disable():
    print('Disabling Calibration',file=sys.stderr)
    loadNGOPTS(netGuardian)
    disableCalibration(netGuardian)
    write_ng(netGuardian)
    flash('Disabled Calibration')
    return redirect(url_for('rtu.test'))

@bp.route('/restore')
def restoreNgopts():
    restore(netGuardian)
    write_ng(netGuardian)
    return redirect(url_for('rtu.test'))

@bp.route('/psucalibrate',methods=('GET','POST'))
def calibratepsu():
    powerOn(powerSupply)
    setValue(powerSupply,1.0,VOLTAGE)
    time.sleep(15)
    low = readValue(powerSupply,VOLTAGE)

    setValue(powerSupply,44.0,VOLTAGE)
    time.sleep(20)
    high = readValue(powerSupply,VOLTAGE)

    x = 44.0 - 1.0
    y = high - low
    slope = y / x
    yint = ((44 - high) * slope) + 0.03
    powerSupply['slope'] = slope
    powerSupply['yint'] = yint 
    setValue(powerSupply,5.0,VOLTAGE)
    powerOff(powerSupply)
    powerSupply['calibrated'] = True
    flash('Calibrated Variable Power Supply, slope {}, yint {}'.format(slope,yint))
    return redirect(url_for('rtu.configure'))

@bp.route('/regulartest',methods=('GET','POST'))
def testloop() :
    global calibrationStage
    global calibrationCycle
    global valueToSet
    global rangeIndex
    global targetVoltage
    global tolerance
    global ngHigh
    global ngLow
    global psHigh
    global psLow
    global index
    global readingLimit
    global startTime
    global maxCount
    global currentCount
    (ngReading,ngTime) = poll_ng(netGuardian) 
    labels.append(int(ngTime - startTime))
    ngVoltageReadings.append(ngReading)
    (psReading,psTime) = readValue(powerSupply,VOLTAGE)
    psuVoltageReadings.append(psReading)
    print("calibrationStage: ",calibrationStage,file=sys.stderr)
    # set middle voltage
    if calibrationStage == 2 :
        targetVoltage = calibrationRanges[rangeIndex]
        print("Setting voltage to: ",targetVoltage,file=sys.stderr)
        rangeIndex = rangeIndex + 3                                                
        setValue(powerSupply,currentValue,CURRENT)
        setValue(powerSupply,targetVoltage,VOLTAGE)
        powerOn(powerSupply)
        calibrationStage = 3
    #wait for voltage to be close
    if (calibrationStage == 3) and ((abs(psuVoltageReadings[len(psuVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)) :
        calibrationStage = 4
        currentCount = 0
    elif calibrationStage == 3 :
        currentCount = currentCount + 1
    if (calibrationStage == 4) and ((abs(ngVoltageReadings[len(ngVoltageReadings) - 1] - targetVoltage) < tolerance) or (currentCount >= maxCount)):     
        currentCount = 0
        psLow = psuVoltageReadings[len(psuVoltageReadings) - 1]
        ngLow = ngVoltageReadings[len(ngVoltageReadings) - 1]
        calibrationStage = 5
    elif calibrationStage == 4 :
        currentCount = currentCount + 1

    #save calibration
    if calibrationStage == 5 : 
        testingData[calibrationCycle]['actual'] = psLow
        testingData[calibrationCycle]['netguardian'] = ngLow
        testingData[calibrationCycle]['error'] = ((ngLow - psLow) / psLow) * 100
        calibrationCycle = calibrationCycle + 1
        calibrationStage = 2


    ngGraph = [] 
    psGraph = []
    times = []
    ngLength = len(ngVoltageReadings)
    if ngLength > readingLimit :
        ngGraph = ngVoltageReadings[ngLength - readingLimit : ngLength]
        psGraph = psuVoltageReadings[ngLength - readingLimit : ngLength]
        times = labels[ngLength - readingLimit : ngLength]
    else:
        ngGraph = ngVoltageReadings
        psGraph = psuVoltageReadings
        times = labels
    return render_template('home/regulartest.html', data = testingData, values = ngGraph, values2 = psGraph, labels = times)

@bp.route('/test',methods=('GET','POST'))
def test():
      values = {}
      if not 'target_voltage' in request.form:
        values['target'] = 10.0
      if request.method == 'POST' :
          #powerOn(powerSupply)
          #powerOff(powerSupply)
          powerOn(powerSupply)
          setValue(powerSupply,0.1,CURRENT)
          time.sleep(1)
          setValue(powerSupply,float(request.form['target_voltage']),VOLTAGE)
          time.sleep(6)
          values['actual'] = readValue(powerSupply,VOLTAGE)
          ng =  poll_ng(netGuardian)

          values['netguardian'] = ng
 
          values['error'] = ((values['netguardian'] - values['actual']) / values['actual']) * 100.0
          
          setValue(powerSupply,5.0,VOLTAGE)
          powerOff(powerSupply)
      return render_template('home/test.html',voltage = values)

@bp.route('/',methods=('GET', 'POST'))
@bp.route('/configure',methods=('GET', 'POST'))
def configure(): 
    if request.method == 'POST':
        netGuardian['ip'] = request.form['ng_ip']
        netGuardian['port'] = request.form['ng_port']
        netGuardian['address'] = request.form['ng_address']
        netGuardian['channels'] = int(request.form['channels'])
        powerSupply['address'] = request.form['psu_address']
        powerSupply['ip'] = request.form['psu_ip']
        powerSupply['port'] = request.form['psu_port']
        calibrationRanges[1] = float(request.form['range_1_low'])
        calibrationRanges[2] = float(request.form['range_1_high'])
        calibrationRanges[4] = float(request.form['range_2_low'])
        calibrationRanges[5] = float(request.form['range_2_high'])
        calibrationRanges[7] = float(request.form['range_3_low'])
        calibrationRanges[8] = float(request.form['range_3_high'])
        calibrationRanges[10] = float(request.form['range_4_low'])
        calibrationRanges[11] = float(request.form['range_4_high'])
        calibrationRanges[13] = float(request.form['range_5_low'])
        calibrationRanges[14] = float(request.form['range_5_high'])
        flash('Saved Values')
    return render_template('home/configure.html')   

@bp.route('/success')
def success():
    writeCalibrationValues(netGuardian,calibrationValues)
    enableCalibration(netGuardian)
    time.sleep(10)
    write_ng(netGuardian)
    setValue(powerSupply,5.0,VOLTAGE)
    powerOff(powerSupply)
    return render_template('home/success.html',data = calibrationData)
    


   
