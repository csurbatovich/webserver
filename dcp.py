import socket
import os
import sqlite3
import time
import sys
import smtplib

import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

smtp_server = "127.0.0.1"
smtp_port = 2225
UDP_PORT = 2002
send_email = "alarm@rtu.com"
receive_email = "monitor@website.com"
bytesToRead = 3

fullUpdateCounter = 0
fullUpdatePeriod = 5

DCPUPDR = 2
DCPFUDR = 3
DACK = 6
DCP_POLL_INTERVAL = 5
commandQueue = []

preAlarm = 0
postAlarm = 0





style.use('fivethirtyeight')

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)
def animate():
    graph_data = open('example.txt','r').read()
    print(graph_data,file=sys.stderr)
    lines = graph_data.split('\n')
    xs = []
    ys = []
    for line in lines:
        if len(line) > 1:
            x, y = line.split(',')
            xs.append(float(x))
            ys.append(float(y))
    ax1.clear()
    ax1.plot(xs, ys)
    #plt.show()
    fig.savefig('webserver/static/plot.png')



def dcpLoop() :

    while 1:
        animate()
        global fullUpdateCounter
        print('in loop',file=sys.stderr)
        conn = sqlite3.connect(
            'instance/webserver.sqlite',
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        conn.row_factory = sqlite3.Row
        db = conn.cursor()
        rtus = db.execute('SELECT id, name, address, ip, port FROM rtu').fetchall()
        print('connected to database',file=sys.stderr)
        print(rtus,file=sys.stderr)
        for rtu in rtus :
            print('in for loop',file=sys.stderr)
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.bind(('',UDP_PORT))
            print('created socket',file=sys.stderr)
            alarmState = db.execute('SELECT display1 FROM rtuData WHERE rtuID=?',(rtu['id'],)).fetchone()
            try :
                preAlarm = alarmState['display1']
            except TypeError :
                preAlarm = alarmState
            # make the message
            message = []
            if fullUpdateCounter >= fullUpdatePeriod :
                print('creating fudr message',file=sys.stderr)
                message = buildPoll(rtu['address'],DCPFUDR)
                bytesToRead = 3
            else :
                print('creating updr message',file=sys.stderr)
                message = buildPoll(rtu['address'],DCPUPDR)
                bytesToRead = 3
            #print('sending message' + message + ' to ' + socket.inet_aton(rtu['ip']),file=sys.stderr)
            print(rtu['ip'])
            print(rtu['port'])
            try :   
                tmp_activity = db.execute('SELECT polling FROM rtuData WHERE rtuID=?',(rtu['id'],)).fetchone()
                activity = tmp_activity['polling']
         
                sock.sendto(bytes(message),(rtu['ip'],rtu['port']))
                print('sent poll message',file=sys.stderr)                                          
                # set timeout

                print('listening on dcp port',file=sys.stderr)
                sock.settimeout(2)
                #listen for response
                tmp_activity = db.execute('SELECT polling FROM rtuData WHERE rtuID=?',(rtu['id'],)).fetchone()
                activity = tmp_activity['polling']
     

 
                netBuffer = bytearray(bytesToRead)
                sock.recv_into(netBuffer,bytesToRead)
                bch = calcBCH(netBuffer,bytesToRead)
                if bch == netBuffer[bytesToRead-1] :
                    num_lines = netBuffer[1]
                    i = 0;
                    while i < num_lines :
                        bytesToRead = 6
                        netBuffer = bytearray(bytesToRead)
                        sock.recv_into(netBuffer,bytesToRead)
                        bch = calcBCH(netBuffer,bytesToRead)
                        if bch == netBuffer[bytesToRead-1] :
                            if netBuffer[0] == 1 :

                                alarmState = int.from_bytes(netBuffer[1:5],byteorder='little')
                                print(alarmState,file=sys.stderr)
                                db.execute(
                                    'UPDATE rtuData SET {}=? WHERE id=?'.format('display' + str(netBuffer[0])),
                                    (alarmState,rtu['id'])
                                    )
                            elif netBuffer[0] == 2 :
                                db.execute(
                                    'UPDATE rtuData SET temperature=?, humidity=? WHERE id=?',
                                    (netBuffer[1],netBuffer[2],rtu['id'])
                                    )
                        else :
                            print("BCH failed",file=sys.stderr)
                        i = i + 1
                    message = buildPoll(rtu['address'],DACK)
                    sock.sendto(bytes(message),(rtu['ip'],rtu['port']))
                    print("setting status active",file=sys.stderr)
                    db.execute(
                        'UPDATE rtuData SET polling=? WHERE id=?',
                        ('active',rtu['id'])
                    )  
                    if activity == 'inactive' :
                        message = """\
                        Subject:  RTU ONLINE
                        

                        rtu {} at {} has come back online""".format(rtu['name'],rtu['ip'])
                        mailServer = smtplib.SMTP(smtp_server, smtp_port)
                        mailServer.sendmail(send_email,receive_email,message)      
            except (socket.timeout,OSError):
                #set timout alarm
                print("setting status inactive",file=sys.stderr)
                db.execute(
                    'UPDATE rtuData SET polling=? WHERE id=?',
                    ('inactive',rtu['id'])
                )     
                print(activity,file=sys.stderr) 
                if activity == 'active' :
                    print("Sending rtu offline email",file=sys.stderr)
                    message = """\
                    Subject:  RTU OFFLINE
                

                    rtu {} at {} has gone offline""".format(rtu['name'],rtu['ip'])
                    mailServer = smtplib.SMTP(smtp_server, smtp_port)
                    mailServer.sendmail(send_email,receive_email,message)          
                print("Timeout",file=sys.stderr)
            conn.commit()
            alarmState2 = db.execute('SELECT display1 FROM rtuData WHERE rtuID=?',(rtu['id'],)).fetchone()
            try :
                postAlarm = alarmState2['display1']
            except TypeError :
                postAlarm = alarmState2
            k = 0
            while k < 32 :
                if (preAlarm & (1<<k)) != (postAlarm & (1<<k)) :
                # display 1 point k alarm changed
                    message = """ """
                    if(postAlarm & (1<<k)) :
                        message = """\
                                  Subject:  ALARM TRIGGERED
                                        

                                  rtu {} point {} is in alarm state""".format(rtu['name'],str(k))
                                        
                    else :
                        message = """\
                                  Subject:  ALARM CLEARED
                                        

                                  rtu {} point {} has cleared""".format(rtu['name'],str(k))                        
                    mailServer = smtplib.SMTP(smtp_server, smtp_port)
                    mailServer.sendmail(send_email,receive_email,message)
                k = k + 1
        time.sleep(DCP_POLL_INTERVAL)

        if fullUpdateCounter >= fullUpdatePeriod :
            fullUpdateCounter = 0
        else :
            fullUpdateCounter = fullUpdateCounter + 1

        conn.close()



    

def buildPoll(address, command):
    buf = []
    buf.append(address)
    buf.append(command)
    buf.append(calculateBCH(address,command))



    return buf


def calcBCH(buffer,length):
    bch = 0
    f_poly = 0x7F
    n_poly = 0x51

    i = 0
    while i < length - 1 :
        bch ^= buffer[i]
        j = 0
        while j < 8 :
            if (bch&1) == 1 :
                bch = (bch >> 1) ^ n_poly
            else :
                bch >>= 1
            j = j + 1
        i = i + 1
    bch ^= f_poly

    return bch


def calculateBCH(address,command):
    bch = 0
    f_poly = 0x7F
    n_poly = 0x51

    bch ^= address
    i = 0
    while i < 8:
        if (bch&1) == 1 :
            bch = (bch >> 1) ^ n_poly
        else :
            bch >>= 1
        i = i + 1
    bch ^= command
    i = 0
    while i < 8:
        if (bch&1) == 1 :
            bch = (bch >> 1) ^ n_poly
        else :
            bch >>= 1
        i = i + 1
    bch ^= f_poly
    return bch
    

    
