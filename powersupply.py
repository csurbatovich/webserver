#listens for incoming messages from the power supply
import socket
import os
import sqlite3
import time
import sys
import struct
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

#define COMMAND_LENGTH 10

#define INPUT_LENGTH 64
DEFAULT_ADDRESS = ord('A')
END_CMD = 0x0A
SET = ord('s')
VOLTAGE =  ord('u')
CURRENT = ord('i')
OUTPUT = ord('o')
ON = ord('1')
OFF = ord('0')
READ = ord('r')
STATE = ord('c')
TIME = ord('a')
RELAY_ON = 0x01
RELAY_OFF = 0x00
style.use('fivethirtyeight')


plt.ylim(0,100)
fig = plt.figure()

ax1 = fig.add_subplot(1,1,1)
def savePlot():
    #load and render voltage/current data
    conn = sqlite3.connect(
        'instance/webserver.sqlite',
        detect_types=sqlite3.PARSE_DECLTYPES
    )
    conn.row_factory = sqlite3.Row
    db = conn.cursor()
    voltages = db.execute('SELECT time,volts FROM VoltageLog LIMIT 5').fetchall()
    xs = []
    ys = []
    for (t,v) in voltages:
        xs.append(t)
        ys.append(v)
    ax1.clear()
    ax1.plot(xs, ys)
    fig.savefig('webserver/imgs/voltage.png')
    
    currents = db.execute('SELECT time,readingValue FROM AnalogReadings LIMIT 5').fetchall()
    xs = []
    ys = []
    for (t,c) in currents:
        xs.append(t)
        ys.append(c)
    ax1.clear()
    ax1.plot(xs, ys)
    fig.savefig('webserver/imgs/actual.png')
def powerOn(powerSupply) :
    message = []
    message.append(ord(powerSupply['address']))
    message.append(SET)
    message.append(OUTPUT)
    message.append(ON)
    message.append(END_CMD)
    print('Turning on Power Supply', file=sys.stderr)
    #print("Sending to powersupply",file=sys.stderr)
    #print(message,file=sys.stderr)
    #send message
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)
    port =  int(powerSupply['port'])
    sock.bind(('',port))
    sock.sendto(bytes(message),(powerSupply['ip'],port))
    #sock.sendto(bytes(message),(netguardian['ip'],int(netguardian['port'])))
    time.sleep(1)
    sock.close() 

def powerOff(powerSupply) :
    message = []
    message.append(RELAY_OFF)
    message.append(ord(powerSupply['address']))
    message.append(SET)
    message.append(OUTPUT)
    message.append(OFF)
    message.append(END_CMD)
    print('Turning off Power Supply', file=sys.stderr)
    # print("Sending to powersupply",file=sys.stderr)
    # print(message,file=sys.stderr)
    #send message
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(5)
    port =  int(powerSupply['port'])
    sock.bind(('',port))
    sock.sendto(bytes(message),(powerSupply['ip'],port))
    #sock.sendto(bytes(message),(netguardian['ip'],int(netguardian['port'])))
    time.sleep(1)
    sock.close() 


def setValue(powerSupply, value, valueType) :

    conn = sqlite3.connect(
        'instance/webserver.sqlite',
        detect_types=sqlite3.PARSE_DECLTYPES
    )
    conn.row_factory = sqlite3.Row
    db = conn.cursor()
    #convert to non float
    message = []   
    if value > 48.0 and valueType is VOLTAGE :
        message.append(RELAY_ON)
        value = value - 48.0
        if powerSupply['external'] is False: 
            powerSupply['external'] = True
            print('Turning fixed power supply on',file=sys.stderr)
        powerSupply['target'] = value

    elif valueType is VOLTAGE :
        message.append(RELAY_OFF)
        if powerSupply['external'] is True :
            print('Turning fixed power supply off',file=sys.stderr)
            powerSupply['external'] = False
        powerSupply['target'] = value

    if valueType is VOLTAGE :
        print('Setting variable power supply voltage to:',value,file=sys.stderr)
    if (powerSupply['calibrated'] is True) and (valueType is VOLTAGE):
        value = (value * powerSupply['slope']) + powerSupply['yint']

    tempInt = int(value * 100) + 100000
    tempBytes = str(tempInt) 
    #create command
    message.append(ord(powerSupply['address']))
    message.append(SET)
    if valueType is VOLTAGE :
        message.append(VOLTAGE)
        message.append(ord('0'))
    elif valueType is CURRENT :
        print('Setting variable power supply current to:',value,file=sys.stderr)
        message.append(CURRENT)
    #copy temp bytes into next 4
    offset = len(tempBytes) - 4
    i = 0
    while i < 4 :
        message.append(ord(tempBytes[offset + i]))
        i = i + 1
    #end message
        #send message

    message.append(END_CMD)
    if valueType is VOLTAGE and powerSupply['target'] < abs(powerSupply['yint']) and powerSupply['yint'] < 0  :
        message = [RELAY_OFF,ord(powerSupply['address']),SET,OUTPUT,OFF,END_CMD]
    #send message
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(10)
    port =  int(powerSupply['port'])
    sock.bind(('',port))

    sock.sendto(bytes(message),(powerSupply['ip'],port))
    time.sleep(1)
    sock.close() 

def readValue(powerSupply,value) :

    message = []
    message.append(ord(powerSupply['address']))
    message.append(READ)
    message.append(value)
    message.append(END_CMD)
    
    #send message
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(10)
    port =  int(powerSupply['port'])
    sock.bind(('',port))
    sock.sendto(bytes(message),(powerSupply['ip'],port))
    netBuffer = bytearray(24)

    sock.recv_into(netBuffer)
    time.sleep(1)
    sock.close() 

    reading = 0.0
    timeStamp = time.time()
    if value is VOLTAGE :
        i = 0
        temp = []
        while (i < 24) and (netBuffer[i] is not ord('u'))  :
            i = i + 1
        i = i + 1
        while (i < 24) and (netBuffer[i] is not ord('\n')) :
            temp.append(chr(netBuffer[i]))
            i = i + 1

        temp2 = ''.join(temp)
        reading = float(int(temp2))/100.0
      
       
        if powerSupply['external'] is True :
            reading = reading + 48.0
    else :
        done = True
        temp = []
        for b in netBuffer[4:7] :
            temp.append(chr(b))
        temp2 = ''.join(temp)
        reading = float(int(temp2))/100.0        
    print('Read',reading,'from power supply',file=sys.stderr)
    return (reading,timeStamp)
    
